#
# Xenomai Real-Time System
#
# Copyright (c) Siemens AG, 2018-2020
#
# Authors:
#  Jan Kiszka <jan.kiszka@siemens.com>
#
# SPDX-License-Identifier: MIT
#

require recipes-kernel/linux/linux-xenomai.inc

SRC_URI_append_amd64 = " git://github.com/xenomai-ci/ipipe.git;protocol=https;nobranch=1"
SRCREV_amd64 ?= "ipipe-core-4.4.238-cip50-x86-26"
PV_amd64 = "4.4.238+"

SRC_URI_append_armhf = " git://github.com/xenomai-ci/ipipe.git;protocol=https;nobranch=1"
SRCREV_armhf ?= "ipipe-core-4.4.238-cip50-arm-11"
PV_armhf = "4.4.238+"

S = "${WORKDIR}/git"
